# Video 4
# Changing buttons
import tkinter
root = tkinter.Tk()
root.geometry('300x300')
root.title('Melody')
root.wm_iconbitmap(bitmap = '@./images/melody.xbm')

#---------------Label--------------------------
text = tkinter.Label(root, text = 'Lets make some noise')
text.pack()
#----------------------------------------------

#----------------Image-------------------------
photo = tkinter.PhotoImage(file = './images/play.png')

#----------------------------------------------
#-----------------Button-----------------------
def play_button():
    print('pretty well')
btn = tkinter.Button(root, image = photo, border = '0', command = play_button)
btn.pack()
#----------------------------------------------
root.mainloop()