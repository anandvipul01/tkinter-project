# Video 5
# PLaying Music
import tkinter
from pygame import mixer
#---------Initializing--Mixer------------------
mixer.init()
#----------------------------------------------
root = tkinter.Tk()
root.geometry('300x300')
root.title('Melody')
root.wm_iconbitmap(bitmap = '@./images/melody.xbm')
#----------------------------------------------


#---------------Label--------------------------
text = tkinter.Label(root, text = 'Lets make some noise')
text.pack()
#----------------------------------------------

#----------------Image-------------------------
playPhoto = tkinter.PhotoImage(file = './images/play.png')

#----------------------------------------------
#-----------------Button-----------------------
def play_music():
    mixer.music.load('journey.wav')
    mixer.music.play()

playBtn = tkinter.Button(root, image = playPhoto, border = '0', command = play_music)
playBtn.pack()
#----------------------------------------------
root.mainloop()