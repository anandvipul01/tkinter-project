# Video 3
# Changing text and images
import tkinter
root = tkinter.Tk()
root.geometry('300x300')
root.title('Melody')
root.wm_iconbitmap(bitmap = '@./images/melody.xbm')

text = tkinter.Label(root, text = 'Lets make some noise')
text.pack()

photo = tkinter.PhotoImage(file = './images/play.png')
labelphoto = tkinter.Label(root, image = photo)
labelphoto.pack()



root.mainloop()