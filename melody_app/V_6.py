# Video 5
# Adding stop button
import tkinter
from pygame import mixer
#---------Initializing--Mixer------------------
mixer.init()
#----------------------------------------------
root = tkinter.Tk()
root.geometry('300x300')
root.title('Melody')
root.wm_iconbitmap(bitmap = '@./images/melody.xbm')
#----------------------------------------------


#---------------Label--------------------------
text = tkinter.Label(root, text = 'Lets make some noise')
text.pack()
#----------------------------------------------

#----------------Image-------------------------
playPhoto = tkinter.PhotoImage(file = './images/play.png')
stopPhoto = tkinter.PhotoImage(file = './images/stop.png')
#----------------------------------------------
#-----------------Button-----------------------
def play_music():
    mixer.music.load('journey.wav')
    mixer.music.play()

def stop_music():
    mixer.music.stop()

playBtn = tkinter.Button(root, image = playPhoto, border = '0', command = play_music)
stopBtn = tkinter.Button(root, image = stopPhoto, border = '0', command = stop_music)
playBtn.pack()
stopBtn.pack()
#----------------------------------------------
root.mainloop()