# Video 8
# Adding a menu bar
# Menu -> Submenu - Cascade -> Command
import tkinter
from pygame import mixer
#---------Initializing--Mixer------------------
mixer.init()
#----------------------------------------------
root = tkinter.Tk()
#----------------------Menu--------------------
menubar = tkinter.Menu(root) # Empty menu bar
root.config(menu = menubar)

#creating a submenu
submenu = tkinter.Menu(menubar, tearoff = 0)
menubar.add_cascade(label = 'File', menu = submenu)
submenu.add_command(label = 'Open')
submenu.add_command(label = 'Exit')

submenu2 = tkinter.Menu(menubar, tearoff = 0)
menubar.add_cascade(label = 'Help', menu = submenu2)
submenu2.add_command(label = 'About Us')




#-----------------------------------------------
root.geometry('300x300')
root.title('Melody')
root.wm_iconbitmap(bitmap = '@./images/melody.xbm')
#----------------------------------------------


#---------------Label--------------------------
text = tkinter.Label(root, text = 'Lets make some noise')
text.pack()
#----------------------------------------------

#----------------Image-------------------------
playPhoto = tkinter.PhotoImage(file = './images/play.png')
stopPhoto = tkinter.PhotoImage(file = './images/stop.png')
#----------------------------------------------
#-----------------Button-----------------------
def play_music():
    mixer.music.load('journey.wav')
    mixer.music.play()

def stop_music():
    mixer.music.stop()

playBtn = tkinter.Button(root, image = playPhoto, border = '0', command = play_music)
stopBtn = tkinter.Button(root, image = stopPhoto, border = '0', command = stop_music)
playBtn.pack()
stopBtn.pack()
#----------------------------------------------
#-----------------Volume-Scale-----------------
def set_vol(val):
    mixer.music.set_volume(int(val)/100)

scale = tkinter.Scale(root, from_=0, to=100, orient = tkinter.HORIZONTAL, command = set_vol)
scale.set(50)
mixer.music.set_volume(50)
scale.pack()

#----------------------------------------------




root.mainloop()