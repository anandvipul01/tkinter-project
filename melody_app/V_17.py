# Video 8
# Adding prettifying
import tkinter
import tkinter.messagebox
from pygame import mixer
from tkinter import filedialog
import os

def show_details():
    text['text'] = 'Playing' + ' - ' + os.path.basename(filename)
        

def about_us():
    tkinter.messagebox.showinfo('Melody App', 'Youtube Tutorial Project @buildwithpython ')
    #tkinter.messagebox.showerror('Our Title', 'this is the information we wast to show')
    #tkinter.messagebox.showwarning('Melody App', 'Tutorial Project @Vipul_Anand')

def play_music():
    global paused
    if paused:
        paused = False
        mixer.music.unpause()
        statusbar['text'] = 'Music Unpaused'            
    else:
        try:
            mixer.music.load(filename)
            mixer.music.play()
            statusbar['text'] = 'Playing Music - ' + os.path.basename(filename)
            show_details()
        except:
            tkinter.messagebox.showerror('File Not Found', 'Melody Couldn\'t find the file please check again')
        
    

def stop_music():
    mixer.music.stop()
    statusbar['text'] = 'Music Stopped'
paused = False


def pause_music():
    global paused
    paused = True
    paused = tkinter.TRUE
    mixer.music.pause()
    statusbar['text'] = 'Music Paused'


def set_vol(val):
    mixer.music.set_volume(int(val)/100)
    
    
def browse_file():
    global filename
    filename = filedialog.askopenfilename()
    

def rewind_music():
    play_music()
    statusbar['text'] = 'Music Rewinded'
muted = False
def mute_music():
    global muted
    
    if muted:
        muted = False
        mixer.music.set_volume(0.5)
        volumeBtn.configure(image = volumePhoto)
        scale.set(50)
        #mixer.set_volume(volume
    else:
        muted = True
        mixer.music.set_volume(0)
        volumeBtn.configure(image = mutePhoto)
        scale.set(0)

#---------Initializing--Mixer------------------
mixer.init()
#----------------------------------------------
root = tkinter.Tk()

#---------------Label--------------------------
text = tkinter.Label(root, text = 'Lets make some noise')
text.pack(pady = 10)
music_length = tkinter.Label(root, text = 'Total Length: 00:00')
music_length.pack(pady = 10)

#----------------------------------------------

middle_frame = tkinter.Frame(root, borderwidth = 1)
middle_frame.pack(padx = 10, pady = 10)
bottom_frame = tkinter.Frame(root, borderwidth = 1)
bottom_frame.pack(padx = 10, pady = 10)

#-----------------------------------------------
#root.geometry('300x300')
root.title('Melody')
root.wm_iconbitmap(bitmap = '@./images/melody.xbm')
#----------------------------------------------

#----------------------Menu--------------------
menubar = tkinter.Menu(root) # Empty menu bar
root.config(menu = menubar)

#creating a submenu
submenu = tkinter.Menu(menubar, tearoff = 0)
menubar.add_cascade(label = 'File', menu = submenu)
submenu.add_command(label = 'Open', command = browse_file)
submenu.add_command(label = 'Exit', command = root.destroy)

submenu2 = tkinter.Menu(menubar, tearoff = 0)
menubar.add_cascade(label = 'Help', menu = submenu2)
submenu2.add_command(label = 'About Us', command = about_us)

#----------------Image-------------------------
playPhoto = tkinter.PhotoImage(file = './images/play.png')
stopPhoto = tkinter.PhotoImage(file = './images/stop.png')
pausePhoto = tkinter.PhotoImage(file = './images/pause.png')
rewindPhoto = tkinter.PhotoImage(file = './images/rewind.png')
mutePhoto = tkinter.PhotoImage(file = './images/mute.png')
volumePhoto = tkinter.PhotoImage(file = './images/volume.png')
#----------------------------------------------


#-----------------Button-----------------------
playBtn = tkinter.Button(middle_frame, image = playPhoto, border = '0', command = play_music)
stopBtn = tkinter.Button(middle_frame, image = stopPhoto, border = '0', command = stop_music)
pauseBtn = tkinter.Button(middle_frame, image = pausePhoto, border = '0', command = pause_music)
rewindBtn = tkinter.Button(bottom_frame, image = rewindPhoto, border = '0', command = rewind_music)
volumeBtn = tkinter.Button(bottom_frame, image = volumePhoto, border = '0', command = mute_music)
playBtn.grid(row = 0, column = 0)
stopBtn.grid(row = 0, column = 2)
pauseBtn.grid(row = 0, column = 1)
rewindBtn.grid(row = 0, column = 2)
volumeBtn.grid(row = 0, column = 0)
#----------------------------------------------
#-----------------Volume-Scale-----------------


scale = tkinter.Scale(bottom_frame, from_=0, to=100, orient = tkinter.HORIZONTAL, command = set_vol)
scale.set(50)
mixer.music.set_volume(.50)
scale.grid(row = 0, column = 1)

#----------------------------------------------
#-----------StatusBar--------------------------
statusbar = tkinter.Label(root, text = 'Welcome to Melody', relief = tkinter.SUNKEN, anchor = tkinter.W)
statusbar.pack(side = tkinter.BOTTOM, fill = tkinter.X)
#----------------------------------------------



root.mainloop()