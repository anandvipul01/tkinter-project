# Video 20
# Adding Listbox
import tkinter
import tkinter.messagebox  
from pygame import mixer
from tkinter import filedialog
from mutagen.mp3 import MP3
import os
import time
import threading

def show_details():
    global timeformat
    global filename
    text['text'] = 'Playing' + ' - ' + os.path.basename(filename)
    file_data = os.path.splitext(filename)
    if file_data[1] == '.mp3':
        t_length = MP3(filename).info.length
        print(t_length)
    else:
        s_f = mixer.Sound(filename)
        t_length = s_f.get_length()    
    mins,secs = divmod(t_length,60)
    mins = round(mins)
    secs = round(secs)
    timeformat = '{:02d}:{:02d}'.format(mins, secs)
    music_length['text'] = ' Total Time:\t' + timeformat
    t1 = threading.Thread(target = start_count,args = (t_length,))
    t1.start()
    
def start_count(t):
    global paused
    while t >= 0 and mixer.music.get_busy():
        if paused:
            continue
        else:
            mins,secs = divmod(t,60)
            mins = round(mins)
            secs = round(secs)
            timeformat = '{:02d}:{:02d}'.format(mins, secs) 
            current_time['text'] = 'Current Time: ' + timeformat
            time.sleep(1)
            t -= 1            
        

def about_us():
    tkinter.messagebox.showinfo('Melody App', 'Youtube Tutorial Project @buildwithpython ')
    #tkinter.messagebox.showerror('Our Title', 'this is the information we wast to show')
    #tkinter.messagebox.showwarning('Melody App', 'Tutorial Project @Vipul_Anand')

def play_music():
    global paused
    if paused:
        paused = False
        mixer.music.unpause()
        statusbar['text'] = 'Music Unpaused'            
    else:
        try:
            mixer.music.load(filename)
            mixer.music.play()
            statusbar['text'] = 'Playing Music - ' + os.path.basename(filename)
            show_details()
        except NameError:
            tkinter.messagebox.showerror('File Not Found', 'Melody Couldn\'t find the file please check again')
        
    

def stop_music():
    mixer.music.stop()
    statusbar['text'] = 'Music Stopped'
paused = False


def pause_music():
    global paused
    paused = True
    paused = tkinter.TRUE
    mixer.music.pause()
    statusbar['text'] = 'Music Paused'


def set_vol(val):
    mixer.music.set_volume(int(val)/100)
    
    
def browse_file():
    global filename
    filename = filedialog.askopenfilename()
    

def rewind_music():
    play_music()
    statusbar['text'] = 'Music Rewinded'
muted = False
def mute_music():
    global muted
    
    if muted:
        muted = False
        mixer.music.set_volume(0.5)
        volumeBtn.configure(image = volumePhoto)
        scale.set(50)
        #mixer.set_volume(volume
    else:
        muted = True
        mixer.music.set_volume(0)
        volumeBtn.configure(image = mutePhoto)
        scale.set(0)

#---------Initializing--Mixer------------------
mixer.init()
#----------------------------------------------
root = tkinter.Tk()
#-----------StatusBar--------------------------
statusbar = tkinter.Label(root, text = 'Welcome to Melody', relief = tkinter.SUNKEN, anchor = tkinter.W)
statusbar.pack(side = tkinter.BOTTOM, fill = tkinter.X)
#----------------------------------------------
#-----------------Frame-----------------------------
left_frame = tkinter.Frame(root)
right_frame = tkinter.Frame(root)
left_frame.pack(side = tkinter.LEFT, padx = 10)
right_frame.pack(side = tkinter.RIGHT)
top_frame = tkinter.Frame(right_frame)
top_frame.pack()
middle_frame = tkinter.Frame(right_frame, borderwidth = 1)
middle_frame.pack(padx = 10, pady = 10)
bottom_frame = tkinter.Frame(right_frame, borderwidth = 1)
bottom_frame.pack(padx = 10, pady = 10)
#----------------------------------------------



#---------------Label--------------------------
text = tkinter.Label(top_frame, text = 'Lets make some noise')
text.pack(pady = 10)
music_length = tkinter.Label(top_frame, text = 'Total Length: --/--')
music_length.pack(pady = 1)
current_time = tkinter.Label(top_frame, text = 'Current Time: --/--', relief = tkinter.GROOVE)
current_time.pack(pady = 1)
lb1 = tkinter.Listbox(left_frame)
lb1.insert(0,'Song1')
lb1.insert(1, 'Song2')
lb1.pack()
#----------------------------------------------



#-----------------------------------------------
#root.geometry('300x300')
root.title('Melody')
root.wm_iconbitmap(bitmap = '@./images/melody.xbm')
#----------------------------------------------

#----------------------Menu--------------------
menubar = tkinter.Menu(root) # Empty menu bar
root.config(menu = menubar)

#-------------creating a submenu---------------
submenu = tkinter.Menu(menubar, tearoff = 0)
menubar.add_cascade(label = 'File', menu = submenu)
submenu.add_command(label = 'Open', command = browse_file)
submenu.add_command(label = 'Exit', command = root.destroy)

submenu2 = tkinter.Menu(menubar, tearoff = 0)
menubar.add_cascade(label = 'Help', menu = submenu2)
submenu2.add_command(label = 'About Us', command = about_us)

#----------------Image-------------------------
playPhoto = tkinter.PhotoImage(file = './images/play.png')
stopPhoto = tkinter.PhotoImage(file = './images/stop.png')
pausePhoto = tkinter.PhotoImage(file = './images/pause.png')
rewindPhoto = tkinter.PhotoImage(file = './images/rewind.png')
mutePhoto = tkinter.PhotoImage(file = './images/mute.png')
volumePhoto = tkinter.PhotoImage(file = './images/volume.png')
#----------------------------------------------


#-----------------Button-----------------------
playBtn = tkinter.Button(middle_frame, image = playPhoto, border = '0', command = play_music)
stopBtn = tkinter.Button(middle_frame, image = stopPhoto, border = '0', command = stop_music)
pauseBtn = tkinter.Button(middle_frame, image = pausePhoto, border = '0', command = pause_music)
rewindBtn = tkinter.Button(bottom_frame, image = rewindPhoto, border = '0', command = rewind_music)
volumeBtn = tkinter.Button(bottom_frame, image = volumePhoto, border = '0', command = mute_music)
add_playlist_btn = tkinter.Button(left_frame, text = '+ Add')
sub_playlist_btn = tkinter.Button(left_frame, text = '- Sub')
playBtn.grid(row = 0, column = 0)
stopBtn.grid(row = 0, column = 2)
pauseBtn.grid(row = 0, column = 1)
rewindBtn.grid(row = 0, column = 2)
volumeBtn.grid(row = 0, column = 0)
add_playlist_btn.pack(side = tkinter.LEFT, padx = 10)
sub_playlist_btn.pack(side = tkinter.LEFT)
#----------------------------------------------
#-----------------Volume-Scale-----------------


scale = tkinter.Scale(bottom_frame, from_=0, to=100, orient = tkinter.HORIZONTAL, command = set_vol)
scale.set(50)
mixer.music.set_volume(.50)
scale.grid(row = 0, column = 1)

#----------------------------------------------


def on_closing():
    stop_music()
    root.destroy()
root.protocol('WM_DELETE_WINDOW',on_closing)
root.mainloop()