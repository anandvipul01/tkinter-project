import tkinter
root = tkinter.Tk()

topframe = tkinter.Frame(root)
topframe.pack()

bottomframe = tkinter.Frame(root)
bottomframe.pack(side = tkinter.BOTTOM)

button1 = tkinter.Button(topframe, text = 'Button1', fg = 'red')
button2 = tkinter.Button(topframe, text = 'Button2', fg = 'blue')
button3 = tkinter.Button(topframe, text = 'Button3', fg = 'green')
button4 = tkinter.Button(bottomframe, text = 'Button4', fg = 'purple')

button1.pack(side = tkinter.RIGHT)
button2.pack(side = tkinter.RIGHT)
button3.pack(side = tkinter.RIGHT)
button4.pack(side = tkinter.LEFT)

root.mainloop()