import tkinter
root = tkinter.Tk()


def leftclick(event):
    print('I am the left clicked')

def middleclick(event):
    print('I am the middle clicked')

    
def rightclick(event):
    print('I am the right clicked')

frame = tkinter.Frame(root, width = 300, height = 250)
frame.bind("<Button-1>",leftclick)
frame.bind("<Button-2>",middleclick)
frame.bind("<Button-3>",rightclick)
frame.pack()


root.mainloop()