# Grid Layout
import tkinter
root = tkinter.Tk()
#thelabel = tkinter.Label(root , text = "Hello Vipul !")
#thelabel.pack()


label_1 = tkinter.Label(root, text = 'Name ')
label_2 = tkinter.Label(root, text = 'Password ')

entry_1 = tkinter.Entry(root)
entry_2 = tkinter.Entry(root)

label_1.grid(row = 0, column = 0, sticky = tkinter.E)
entry_1.grid(row = 0, column = 1)

label_2.grid(row = 1, column = 0, sticky = tkinter.E)
entry_2.grid(row = 1, column = 1)

c = tkinter.Checkbutton(root, text = 'Keep me logged in')
c.grid(columnspan = 2)

root.mainloop()