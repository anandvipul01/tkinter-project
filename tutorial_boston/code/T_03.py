import tkinter
root = tkinter.Tk()
one = tkinter.Label(root, text = 'one', bg = 'red', fg = 'white')
one.pack()

two = tkinter.Label(root, text = 'two', bg = 'green', fg = 'white')
two.pack(fill = tkinter.X)

three = tkinter.Label(root, text = 'one', bg = 'blue', fg = 'white')
three.pack(side = tkinter.LEFT, fill = tkinter.Y)

root.mainloop()